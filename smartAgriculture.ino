//
//  smartAgriculture.ino
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 14.03.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

// Libraries
#include <OneWire.h>
#include <DallasTemperature.h>

// Port definitions
int ecData = A0;
int ecGround = A1;
int ecPower = A2;
int phPin = A3;
int wLevelPin = A4;
int tempPin = 2;

// Temp. Sensor Definition
OneWire oneWire(tempPin);
DallasTemperature sensors(&oneWire);

// EC Conversations
int r1 = 1025;
int rPlugs = 25;
float tempCoef = 0.019;
float K = 1.48;
float Vin = 5;
float buffer = 0;
float rFluid = 0;

// PH Sensor
unsigned long int avgValue;  //Store the average value of the sensor feedback
float b;
int buf[10],temp;

float read_EC = 0;
float read_Temp = 0;
float read_PH = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(ecData, INPUT);
  pinMode(ecGround, OUTPUT);
  pinMode(ecPower, OUTPUT);
  digitalWrite(ecGround, LOW);

  delay(100);
  sensors.begin();
  Serial.println("Started Arduino");
  delay(100);
}

void loop() {
  // put your main code here, to run repeatedly:
  startMeasurements();
}

void startMeasurements() {
  getTemp();
  getConductivity();
  getPH();
  sendToConsole();
  delay(5000);
}

void sendToConsole() {
  Serial.print("ec:");
  Serial.println(read_EC);
  Serial.print("ph:");
  Serial.println(read_PH);
  Serial.print("temp:");
  Serial.println(read_Temp);
}

void getTemp() {
  sensors.requestTemperatures();
  read_Temp = sensors.getTempCByIndex(0);
}

void getConductivity() {
  digitalWrite(ecPower, HIGH);
  rFluid = analogRead(ecData);
  digitalWrite(ecPower, LOW);

  // Calculation
  float Vd = (Vin*rFluid)/1024.0;
  float Rc = ((Vd*r1)/(Vin-Vd))-rPlugs;
  float EC = 1000/(Rc*K);
  read_EC = EC/(1+tempCoef*(tempCoef-25.0));
}

void getPH() {
  for(int i=0;i<10;i++)       //Get 10 sample value from the sensor for smooth the value
  { 
    buf[i]=analogRead(phPin);
    delay(10);
  }
  for(int i=0;i<9;i++)        //sort the analog from small to large
  {
    for(int j=i+1;j<10;j++)
    {
      if(buf[i]>buf[j])
      {
        temp=buf[i];
        buf[i]=buf[j];
        buf[j]=temp;
      }
    }
  }
  avgValue=0;
  for(int i=2;i<8;i++)                      //take the average value of 6 center sample
    avgValue+=buf[i];
  float phValue=(float)avgValue*5.0/1024/6; //convert the analog into millivolt
  read_PH=3.5*phValue;                      //convert the millivolt into pH value
}

